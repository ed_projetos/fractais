#include "pen/cpen.h"
#include <stdlib.h>

void linhac(int lado){

    if(lado < 5)
        return;
    pen_set_color(rand()%255, rand()%255, rand()%255);

    pen_walk(lado);
    pen_right(90);
    linhac(lado - 5);
}

void arvore_fractal(float tamanho_galho) {
    if(tamanho_galho > 10) {
        pen_walk(tamanho_galho);

        int x = pen_get_x(), y = pen_get_y();
        float angulo = pen_get_heading();

        pen_right(30);
        arvore_fractal(tamanho_galho*0.7);

        pen_set_xy(x, y);

        pen_set_heading(angulo);

        pen_left(30);
        arvore_fractal(tamanho_galho*0.7);
    }
}

int main(){
    pen_open(800, 600);
    //coloca o pincel na posicao x 300 y 500
    pen_set_xy(400, 600);

    //faz o pincel apontar pra direita
    pen_set_heading(90);
    //se speed = 0 entao ele faz o mais rapido possivel
    //se foi a partir de 1 ele controla a velocidade
    pen_set_speed(30);
    
    arvore_fractal(200);
    
    //espera clicar no botao de fechar
    pen_done();
    pen_close();
    return 0;
}












