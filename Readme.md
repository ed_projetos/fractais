# Fractais

## Objetivo

O seu objetivo é aprender recursão para implementar o código que produzirá os fractais da pasta imagens. Faça o máximo que puder. Se enganchar em algum, pesquise na internet.

## Instalação

A biblioteca gráfica já está inclusa nesse projeto, não sendo necessário instalá-la ou configurá-la.

Entretanto, se houver algum problema de compilação do projeto da forma como ele está agora, provavelmente a biblioteca que está nesse projeto não é compatível com a versão do C no seu computador. Para resolver, instale uma versão compatível com o comando abaixo.


        sudo apt-get install libsfml-dev


## Uso

A biblioteca da caneta para C está em pen/cpen.h. A biblioteca para C++ está em pen/pen.h.
O projeto do QtCreator já inclui essas duas bibliotecas.
Se quiser compilar na mão, basta criar o arquivo fractal.c ou fractal.cpp e digitar:
    
        make fractal

O Makefile já contém as intruções de compilação.
    
Na pasta imagens você tem exemplos de fractais a serem implementados.

### Funcões em C

```c
	// cria e inicializa a caneta e abre uma janela com a largura e altura informados
	void pen_open(int largura, int altura);
	
	// finaliza a caneta. qualquer comando após esse dará erro.
	void pen_close();
    
    //anda pra frente e se o valor for negativo para trás
    void pen_walk(float distance);
    
    // desenha um círculo com o raio informado ao redor da posição atual.
    void pen_circle(float raio);

	// rotaciona a direção da caneta segundo o ângulo dado (em graus)
    void pen_rotate(float angulo);

    //vira pra esquerda (em graus)
    void pen_left(float angulo);

    //vira pra direita (em graus)
    void pen_right(float angulo);

    //limpa todos os tracos da tela
    void pen_clear();

    //levanta a caneta do papel
    void pen_up();

    //abaixa a caneta no papel
    void pen_down();

    //vai para essa posicao da tela riscando o papel se down
    void pen_move(float x, float y);

    //espera tantos segundos
    void pen_wait(int seconds);

    //espera ate clicar em fechar
    void pen_done();

    //muda a cor de risco
    void pen_set_color(int R, int G, int B);
    
    //muda a cor de fundo
    void pen_set_back_color(int R, int G, int B);

    //geran um numero aleatorio
    int pen_rand();

    //GETTERS and SETTERS

    //muda x e y sem riscar
    void  pen_set_xy(float x, float y);
    
    //muda o angulo de orientacao em graus
    //segue o cartesiano, 0 é a direita, cresce no sentido antihorário
    void  pen_set_heading(float angulo);    

    //muda a velocidade
    //se velocidade = 0, então ele não mostra o desenho até que esteja terminado
    //se velocidade > 0, speed pode ser qualquer valor inteiro
    void  pen_set_speed(int velocidade);

    //muda a espessura da linha
    void  setThickness  (int espessura);

    float pen_get_x();        
    float pen_get_y();
    int   pen_get_speed();
    float pen_get_heading();
    int   pen_get_thickness();
```

### Métodos em C++

```c++
    //anda pra frente e se o valor for negativo para trás
    void walk(float distance);

    void rotate(float angulo);

    //vira pra esquerda
    void left(float angulo);

    //vira pra direita
    void right(float angulo);

    //limpa todos os tracos da tela
    void clear();

    //levanta a caneta do papel
    void up();

    //abaixa a caneta no papel
    void down();

    //vai para essa posicao da tela riscando o papel se down
    void move(float x, float y);

    //espera tantos segundos
    void wait(int seconds);

    //espera ate clicar em fechar
    void wait();

    //muda a cor de risco
    void setColor(int R, int G, int B);
    
    //muda a cor de fundo
    void setBackColor(int R, int G, int B);

    //geran um numero aleatorio
    static int rand();

    //GETTERS and SETTERS

    //muda x e y sem riscar
    void  setXY         (float x, float y);
    
    //muda o angulo de orientacao em graus
    //segue o cartesiano, 0 é a direita, cresce no sentido antihorário
    void  setHeading    (float angulo);    

    //muda a velocidade
    //se velocidade = 0, então ele não mostra o desenho até que esteja terminado
    //se velocidade > 0, speed pode ser qualquer valor inteiro
    void  setSpeed      (int velocidade);

    //muda a espessura da linha
    void  setThickness  (int espessura);

    float getX();        
    float getY();
    int   getSpeed();
    float getHeading();
    int   getThickness();
```

